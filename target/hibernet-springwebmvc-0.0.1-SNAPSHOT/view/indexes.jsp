<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>shopping</title>
    <link href="./resources/css/index.css" rel="stylesheet" />
    <link rel="icon" href="https://img.favpng.com/10/5/4/shopping-cart-online-shopping-icon-png-favpng-0K6dNnKDELGHe0XfMKsR04fPH.jpg" />

    <link rel="stylesheet" href="./style.css">
    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">

</head>

<body>    <header>
        <div class="logo"><a href="#">ShoPping</a></div>
        <div class="menu">
            <a href="">
                <ion-icon name="close" class="close"></ion-icon>
            </a>
            <ul> <li><a href="login" class="under">HOME</a></li>
                <li><a href="#" class="under">SHOP</a></li>
                <li><a href="#" class="under">OUR PRODUCTS</a></li>
                <li><a href="contact" class="under">CONTACTus</a></li>
                <nav class="navbar">
            <a href="about">about</a>
            </nav>            </ul>
        </div>
        <div class="search">
            <a href=""><input type="text" placeholder="search clients" id="input">
                <ion-icon class="s" name="search"></ion-icon>
            </a>
        </div>
        <div class="heading">
            <ul>
                <li><a href="login" class="under">HOME</a></li>
                <li><a href="#" class="under">SHOP</a></li>
                <li><a href="#" class="under">OUR PRODUCTS</a></li>
                <!-- <li><a href="#" class="under">CONTACT US</a></li> -->
                <!-- <li><a href="#" class="under">ABOUT US</a></li> -->
                <nav class="navbar">
                    <a href="about">ABOUT US</a>
                    <a href="contact">CONTACT</a>

                </nav>
            </ul>
        </div>
        <div class="heading1">
            <ion-icon name="menu" class="ham"></ion-icon>
        </div>
    </header>
    <section>
        <div class="section">
          
            <div class="section1">
                <div class="img-slider">
                    <img src="https://m.media-amazon.com/images/I/71uT4qs00BL._SL1080_.jpg"
                        alt="" class="img">
                    <img src="https://m.media-amazon.com/images/I/71UeuzaMGML._SY879_.jpg"
                        alt="" class="img">
                    <img src="https://m.media-amazon.com/images/I/51R6Ygn0WkL.jpg"
                        alt="" class="img">
                    <img src="https://m.media-amazon.com/images/I/81wRZznAFtL._UY879_.jpg"
                        alt="" class="img">
                    <img src="https://images-eu.ssl-images-amazon.com/images/I/41NNWAx-XQL._SX300_SY300_QL70_FMwebp_.jpg"
                        alt="" class="img">
                </div>
            </div>
            <h3>OUR PRODUCTS</h3>
            <div class="section2">
                <div class="container">
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/61pmIvqpXLL._UL1100_.jpg"
                                alt=""></div>
                        <div class="name">SHOES</div>
                        <div class="price">1099</div>
                        <div class="info">Mens Trainers Hi-Tec Untra Terra Moutain.</div>
                    </div>
                    <div class="items">
                        <div class="img img2"><img
                                src="https://m.media-amazon.com/images/I/61hh8wS16wL._UY879_.jpg"
                                alt=""></div>
                        <div class="name">MEN's T-SHIRT</div>
                        <div class="price">634</div>
                        <div class="info">Men's Basic V-neck Short Sleeve T-shirt.</div>
                    </div>
                    <div class="items">
                        <div class="img img3"><img
                                src="https://m.media-amazon.com/images/I/81oG9SoTqsL._UY879_.jpg"
                                alt=""></div>
                        <div class="name">JEANS</div>
                        <div class="price">1,999</div>
                        <div class="info">Willex Rain Trousers Size M Black.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://images-eu.ssl-images-amazon.com/images/I/41pSa6NtsiL._SX300_SY300_QL70_FMwebp_.jpg"
                                alt=""></div>
                        <div class="name">WATCH</div>
                        <div class="price">2,599</div>
                        <div class="info">Fire-Boltt Ninja Calling Bluetooth Calling,Dial Pad,Speaker.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://images-eu.ssl-images-amazon.com/images/I/41ZIcSihDEL._SX300_SY300_QL70_FMwebp_.jpg"
                                alt=""></div>
                        <div class="name">SMART PHONE</div>
                        <div class="price">20,000</div>
                        <div class="info">Samsung Galaxy S22 Ultra 5G.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/71arAQvySlL._SX679_.jpg"
                                alt=""></div>
                        <div class="name">TELEVISION</div>
                        <div class="price">17,998</div>
                        <div class="info">LG 80cm(32 Inches)Wondertainment.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/813gV9tGXeL._UY879_.jpg"
                                alt=""></div>
                        <div class="name">HOODIES</div>
                        <div class="price">670</div>
                        <div class="info">Hoodie Sweatshirt Casual Jacket Coat.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/71Dt-4f40HL._SY879_.jpg"
                                alt=""></div>
                        <div class="name">DINNER SET</div>
                        <div class="price">3,299</div>
                        <div class="info">Cello ImperialFlower Opalware Dinner Set.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/71DTzaVYMPS._SX679_.jpg"
                                alt=""></div>
                        <div class="name">BLANKETS</div>
                        <div class="price">999</div>
                        <div class="info">Tiki Mask Style Tropical Flowers flowers Blanket.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://images-eu.ssl-images-amazon.com/images/I/517bXfKQ1AL._SX300_SY300_QL70_FMwebp_.jpg"
                                alt=""></div>
                        <div class="name">LAPTOP</div>
                        <div class="price">45,599</div>
                        <div class="info">Laptop, Windows 11 Home PC 2.8GHz 6GB.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/S/al-eu-726f4d26-7fdb/01ee4858-59c7-4922-9319-25c5b7e47d10._CR0,0,1200,628_SX810_QL70_.jpg"
                                alt=""></div>
                        <div class="name">MICROWAVE</div>
                        <div class="price">3000</div>
                        <div class="info">Microwave Oven Red Small Kitchen Appliance.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/41G+LHRWs6L._SY300_SX300_.jpg"
                                alt=""></div>
                        <div class="name">COFFEE MAKER</div>
                        <div class="price">2907</div>
                        <div class="info">Ninja 12-Cup Coffee Maker.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/91xBAc8d7yL._SX679_.jpg"
                                alt=""></div>
                        <div class="name">BED</div>
                        <div class="price">10,000</div>
                        <div class="info">Modern Full Bed Frame with LED Lights Platform.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/5120ROoJnWL._SX679_.jpg"
                                alt=""></div>
                        <div class="name">AIR CONDITIONER</div>
                        <div class="price">43,000</div>
                        <div class="info">Voltas Inverter Split AC 1.5 Ton 5 Star.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://images-na.ssl-images-amazon.com/images/I/41+CqNWoutS._SX460_BO1,204,203,200_.jpg"
                                alt=""></div>
                        <div class="name">BOOK</div>
                        <div class="price">1,400</div>
                        <div class="info">The Psychology of Self-Esteem by Branded.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/71GxZAYa6UL._UX679_.jpg"
                                alt=""></div>
                        <div class="name">BAG</div>
                        <div class="price">995</div>
                        <div class="info">Backpack 17 Inch Travel Gear Bag Business.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/91yo+G2rJ1L._UY879_.jpg"
                                alt=""></div>
                        <div class="name">SAREES</div>
                        <div class="price">1256</div>
                        <div class="info">SILK SARI TRADITIONAL DESIGNER SAREE.</div>
                    </div>
                    <div class="items">
                        <div class="img img1"><img
                                src="https://m.media-amazon.com/images/I/81p2VIbJAOL._SX679_.jpg"
                                alt=""></div>
                        <div class="name">WATER BOTTLES</div>
                        <div class="price">256</div>
                        <div class="info">OOLACTIVE  Water Bottle with Straw.</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="footer0">
            <h1>ShoPperZ</h1>
        </div>
        <div class="footer1 ">
            Connect with us at<div class="social-media">
                <a href="#">
                    <ion-icon name="logo-facebook"></ion-icon>
                </a>
                <a href="#">
                    <ion-icon name="logo-linkedin"></ion-icon>
                </a>
                <a href="#">
                    <ion-icon name="logo-youtube"></ion-icon>
                </a>
                <a href="#">
                    <ion-icon name="logo-instagram"></ion-icon>
                </a>
                <a href="#">
                    <ion-icon name="logo-twitter"></ion-icon>
                </a>
            </div>
        </div>
        <div class="footer2">
            <div class="client">
                <div class="heading">Clients</div>
                <div class="div">Sell your Clients</div>
                <div class="div">Advertise</div>
                <div class="div">Pricing</div>
                <div class="div">Client Buisness</div>
            </div>
            <div class="services">
                <div class="heading">Services</div>
                <div class="div">Return</div>
                <div class="div">Cash Back</div>
                <div class="div">Affiliate Marketing</div>
                <div class="div">Others</div>
            </div>
            <div class="Company">
                <div class="heading">Company</div>
                <div class="div">Complaint</div>
                <div class="div">Careers</div>
                <div class="div">Affiliate Marketing</div>
                <div class="div">Support</div>
            </div>
            <div class="Get Help">
                <div class="heading">Get Help</div>
                <div class="div">Help Center</div>
                <div class="div">Privacy Policy</div>
                <div class="div">Terms</div>
                <div class="div">Login</div>
            </div>
        </div>
        <div class="credit"> created by || <span>ANISETTI ANUSHA</span> || all rights reserved </div>
    </footer>
    <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
    <script src="./index.js"></script>
</body>

</html>
</head>
<body>

</body>
</html>