<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>about PRSPA&SALONS</title>
<!--  <link href="./resources/css/about.css" rel="stylesheet" />-->
<link rel="icon"
    href="https://cdn-icons-png.flaticon.com/512/189/189664.png" />

</head>
<body background="https://tse4.mm.bing.net/th/id/OIP.qBw8GSd0civ_QjWeT7Da_AHaCs?pid=ImgDet&rs=1;">
<div class="about-section">
<center>
    <h1>ABOUT</h1>
    <h1><b>Relax, Rejuvenate & Repeat</b></h1>
    
    <p> A LOCATION WHERE MINERAL-RICH SPRING WATER [AND SOMETIMES SEA WATER] IS USED TO GIVE MEDICINAL BATHS</p>
    
    </center>
</div>
<p>Our beauty salon offers a stunning complement of therapeutic and rejuvenating face and body beauty 
treatments, using only the best skincare products with the finest ingredients.</p></br></br></br>
            <p>The Spa Beauty Salon team of experienced and highly trained beauty
             therapists will be happy to put together a spa or beauty treatment package designed specifically
              for your needs and to offer you advice on your skin’s requirements. Within the Spa Beauty Salon 
              oasis of calm and tranquility, begin your journey of self-discovery away from the outside world
               where body, mind and soul are nourished and restored, leaving you energised from within and 
               looking great.</p>
           

<!-- <h2 style="text-align:center">Our Team</h2>
<div class="row">
    <div class="column">
        <div class="card">
            <img src="https://image.cnbcfm.com/api/v1/image/105959992-1560253715688rec_asa_code19-20190610-190247-9938-x3.jpg?v=1560253800" alt="Jane" style="width:100%">
            <div class="container">
                <h2>Jane Doe</h2>
                <p class="title">CEO & Founder</p>
                <p>Andy Jassy currently serves as Amazon’s president and CEO, recently taking over from Bezos in July 2021. He also serves
                on the board of directors. Jassy is considered the founder of AWS, which he had led since its 2006 inception—including
                as its CEO,.</p>
                <p>jane@example.com</p>
                <p><button class="button">Contact</button></p>
            </div>
        </div>
    </div>

    <div class="column">
        <div class="card">
            <img src="https://i2.wp.com/www.infostarr.com/wp-content/uploads/2017/12/Jeff-Bezos-Amazon-CEO.jpg?fit=3777%2C2591&ssl=1" alt="Mike" style="width:100%">
            <div class="container">
                <h2>Mike Ross</h2>
                <p class="title">Art Director</p>
                <p>Adam N. Selipsky Chief Executive Officer, Amazon Web Services David A. Zapolsky Senior Vice President, General Counsel
                and Secretary Directors Jeffrey P. Bezos Executive Chair Andy Amazon Web Services from 2003 to 2021.</p>
                <p>mike@example.com</p>
                <p><button class="button">Contact</button></p>
            </div>
        </div>
    </div>

    <div class="column">
        <div class="card">
            <img src="https://tse3.mm.bing.net/th/id/OIP.VJtXChFIIQX8LonZJOK_6AHaEK?pid=ImgDet&rs=1" alt="John" style="width:100%">
            <div class="container">
                <h2>John Doe</h2>
                <p class="title">Designer</p>
                <p>Andrew R. Jassy is an American business executive who has been the president and chief executive officer of Amazon since
                2021. He was previously senior vice president and CEO of Amazon Web Services from 2003 to 2021, and was selected by Jeff
                Bezos during the 2020 fourth quarterly results when Bezos became executive chairman..</p>
                <p>john@example.com</p>
                <p><button class="button">Contact</button></p>
            </div>
        </div>
    </div>
</div>-->
</br></br></br></br></br></br></br></br></br></br>
<center>
<section class="footer">
    <div class="credit">----------------created by |   <span>PR SPA&SALONS</span>   | all rights reserved---------------- </div>

</section>
</center>
</body>
</html>