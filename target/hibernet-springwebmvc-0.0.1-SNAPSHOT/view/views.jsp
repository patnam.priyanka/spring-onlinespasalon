<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>CLIENTS DETAILS</title>
<link href="./resources/css/update.css" rel="stylesheet" />
<link rel="icon"
    href="https://tse4.mm.bing.net/th/id/OIP.cbnlhmON5bmDsryRO73zzQHaHa?pid=ImgDet&rs=1" />

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"crossorigin="anonymous">
</head>
<body>
<div class="header">
		<div class="header-right">
		
			<a class="active" href="login">Home</a> <a href="contact">Contact</a>
			<a href="about">About</a>
			<a href="indexes">Indexes</a>
		
		</div>
	</div>
	<h1 class="container"><center>GET INTO PR SPA&SALONS</center></h1>
	<hr />
	<center>
	<h3 class="container">Clients Available List</h3>
	<a href="views">REFRESH</a></br>
	<a href="insert">ADD</a>
	</center>
	<div class="container" style="width: 400px">
		<table class="table table-striped">
		
			<tr>
				<th>CLIENT ID</th>
				<th>CLIENT NAME</th>
				<th>UPDATE</th>
				<th>DELETE</th>

			</tr>
			<c:forEach items="${clientref}" var="client">
				<tr>
					<td>${client.getId()}</td>
					<td>${client.getName()}</td>
					<td><a href="update?id=${client.getId()}&name=${client.getName()}">update</a></td>
					<td><a href="delete?id=${client.getId()}">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	</br></br></br></br></br></br></br>
	<center>
<section class="footer">
    <div class="credit">----------------created by |   <span>PR SPA&SALONS</span>   | all rights reserved---------------- </div>

</section>
</center>
	
	<!--  <footer style="background-color:;text-align: center">
		<div class="credit">
			created by || <span>ANISETTI ANUSHA</span> || all rights reserved
		</div>
		
		<div
			style="position: fixed; left: 0; bottom: 0; background-color: red; color: white; text-align: center;"
			class="credit">
			</div>
		
	

		<a href="index">shopping.com</a>
		</p>
		<a href="https://www.facebook.com/" class="fa fa-facebook"></a> <a
			href="#" class="fa fa-twitter"></a> <a href="#" class="fa fa-google"></a>
		<a href="#" class="fa fa-linkedin"></a>
		
	</footer>-->
</body>
</html>