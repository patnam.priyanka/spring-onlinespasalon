<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>about PRSPA&SALONS</title>
<!--  <link href="./resources/css/about.css" rel="stylesheet" />-->
<link rel="icon"
    href="https://cdn-icons-png.flaticon.com/512/189/189664.png" />

</head>
<body background="https://tse4.mm.bing.net/th/id/OIP.qBw8GSd0civ_QjWeT7Da_AHaCs?pid=ImgDet&rs=1;">
<div class="about-section">
<center>
    <h1>ABOUT</h1>
    <h1><b>Relax, Rejuvenate & Repeat</b></h1>
    
    <p> A LOCATION WHERE MINERAL-RICH SPRING WATER [AND SOMETIMES SEA WATER] IS USED TO GIVE MEDICINAL BATHS</p>
    
    </center>
</div>
<p>Our beauty salon offers a stunning complement of therapeutic and rejuvenating face and body beauty 
treatments, using only the best skincare products with the finest ingredients.</p></br></br></br>
            <p>The Spa Beauty Salon team of experienced and highly trained beauty
             therapists will be happy to put together a spa or beauty treatment package designed specifically
              for your needs and to offer you advice on your skin’s requirements. Within the Spa Beauty Salon 
              oasis of calm and tranquility, begin your journey of self-discovery away from the outside world
               where body, mind and soul are nourished and restored, leaving you energised from within and 
               looking great.</p>
           

</br></br></br></br></br></br></br></br></br></br>
<center>
<section class="footer">
    <div class="credit">----------------created by |   <span>PR SPA&SALONS</span>   | all rights reserved---------------- </div>

</section>
</center>
</body>
</html>