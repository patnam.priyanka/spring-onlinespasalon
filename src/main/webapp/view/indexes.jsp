<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<style>
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700&display=swap');

:root{
    --green:#a9376c;
    --black:#444;
    --light-color:#777;
    --box-shadow:.5rem .5rem 0 rgba(241, 23, 172, 0.2);
    --text-shadow:.4rem .4rem 0 rgba(0, 0, 0, .2);
    --border:.2rem solid var(--green);
}

*{
    font-family: 'Poppins', sans-serif;
    margin:0; padding: 0;
    box-sizing: border-box;
    outline: none; border: none;
    text-transform: capitalize;
    transition: all .2s ease-out;
    text-decoration: none;
}

html{
    font-size: 62.5%;
    overflow-x: hidden;
    scroll-padding-top: 7rem;
    scroll-behavior: smooth;
}

section{
    padding:2rem 9%;
}

section:nth-child(even){
    background: #f5f5f5;
}

.heading{
    text-align: center;
    padding-bottom: 2rem;
    text-shadow: var(--text-shadow);
    text-transform: uppercase;
    color:var(--black);
    font-size: 5rem;
    letter-spacing: .4rem;
}

.heading span{
    text-transform: uppercase;
    color:var(--green);
}

.btn{
    display: inline-block;
    margin-top: 1rem;
    padding: .5rem;
    padding-left: 1rem;
    border:var(--border);
    border-radius: .5rem;
    box-shadow: var(--box-shadow);
    color:var(--green);
    cursor: pointer;
    font-size: 1.7rem;
    background: #fff;
}

.btn span{
    padding:.7rem 1rem;
    border-radius: .5rem;
    background: var(--green);
    color:#fff;
    margin-left: .5rem;
}

.btn:hover{
    background: var(--green);
    color:#fff;
}

.btn:hover span{
    color: var(--green);
    background:#fff;
    margin-left: 1rem;
}

.header{
    padding:2rem 9%;
    position: fixed;
    top:0; left: 0; right: 0;
    z-index: 1000;
    box-shadow: 0 .5rem 1.5rem rgba(0, 0, 0, .1);
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: #fff;
}

.header .logo{
    font-size: 2.5rem;
    color: var(--black);
}

.header .logo i{
    color: var(--green);
}

.header .navbar a{
    font-size: 1.7rem;
    color: var(--light-color);
    margin-left: 2rem;
}

.header .navbar a:hover{
    color: var(--green);
}

#menu-btn{
    font-size: 2.5rem;
    border-radius: .5rem;
    background: #eee;
    color:var(--green);
    padding: 1rem 1.5rem;
    cursor: pointer;
    display: none;
}

.home{
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    gap:1.5rem;
    padding-top: 10rem;
}

.home .image{
    flex:1 1 45rem;
}

.home .image img{
    width: 100%;
}

.home .content{
    flex:1 1 45rem;
}

.home .content h3{
    font-size: 4.5rem;
    color:var(--black);
    line-height: 1.8;
    text-shadow: var(--text-shadow);
}

.home .content p{
    font-size: 1.7rem;
    color:var(--light-color);
    line-height: 1.8;
    padding: 1rem 0;
}

.icons-container{
    display: grid;
    gap:2rem;
    grid-template-columns: repeat(auto-fit, minmax(20rem, 1fr));
    padding-top: 5rem;
    padding-bottom: 5rem;
}

.icons-container .icons{
    border:var(--border);
    box-shadow: var(--box-shadow);
    border-radius: .5rem;
    text-align: center;
    padding: 2.5rem;
}

.icons-container .icons i{
    font-size: 4.5rem;
    color:var(--green);
    padding-bottom: .7rem;
}

.icons-container .icons h3{
    font-size: 4.5rem;
    color:var(--black);
    padding: .5rem 0;
    text-shadow: var(--text-shadow);
}

.icons-container .icons p{
    font-size: 1.7rem;
    color:var(--light-color);
}

.services .box-container{
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(27rem, 1fr));
    gap:2rem;
}

.services .box-container .box{
    background:#fff;
    border-radius: .5rem;
    box-shadow: var(--box-shadow);
    border:var(--border);
    padding: 2.5rem;
}

.services .box-container .box i{
    color: var(--green);
    font-size: 5rem;
    padding-bottom: .5rem;
}

.services .box-container .box h3{
    color: var(--black);
    font-size: 2.5rem;
    padding:1rem 0;
}

.services .box-container .box p{
    color: var(--light-color);
    font-size: 1.4rem;
    line-height: 2;
}

.about .row{
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    gap:2rem;
}

.about .row .image{
    flex:1 1 45rem;
}

.about .row .image img{
    width: 100%;
}

.about .row .content{
    flex:1 1 45rem;
}

.about .row .content h3{
    color: var(--black);
    text-shadow: var(--text-shadow);
    font-size: 4rem;
    line-height: 1.8;
}

.about .row .content p{
    color: var(--light-color);
    padding:1rem 0;
    font-size: 1.5rem;
    line-height: 1.8;
}

.doctors .box-container{
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(30rem, 1fr));
    gap:2rem;
}

.doctors .box-container .box{
    text-align: center;
    background:#fff;
    border-radius: .5rem;
    border:var(--border);
    box-shadow: var(--box-shadow);
    padding:2rem;
}

.doctors .box-container .box img{
    height: 20rem;
    border:var(--border);
    border-radius: .5rem;
    margin-top: 1rem;
    margin-bottom: 1rem;
}

.doctors .box-container .box h3{
    color:var(--black);
    font-size: 2.5rem;
}

.doctors .box-container .box span{
    color:var(--green);
    font-size: 1.5rem;
}

.doctors .box-container .box .share{
    padding-top: 2rem;
}

.doctors .box-container .box .share a{
    height: 5rem;
    width: 5rem;
    line-height: 4.5rem;
    font-size: 2rem;
    color:var(--green);
    border-radius: .5rem;
    border:var(--border);
    margin:.3rem;
}

.doctors .box-container .box .share a:hover{
    background:var(--green);
    color:#fff;
    box-shadow: var(--box-shadow);
}

.book .row{
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    gap:2rem;
}
 
.book .row .image{
    flex:1 1 45rem;
}

.book .row .image img{
    width: 100%;
}

.book .row form{
    flex:1 1 45rem;
    background: #fff;
    border:var(--border);
    box-shadow: var(--box-shadow);
    text-align: center;
    padding: 2rem;
    border-radius: .5rem;
}

.book .row form h3{
    color:var(--black);
    padding-bottom: 1rem;
    font-size: 3rem;
}

.book .row form .box{
    width: 100%;
    margin:.7rem 0;
    border-radius: .5rem;
    border:var(--border);
    font-size: 1.6rem;
    color: var(--black);
    text-transform: none;
    padding: 1rem;
}

.book .row form .btn{
    padding:1rem 4rem;
}

.review .box-container{
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(27rem, 1fr));
    gap:2rem;
}

.review .box-container .box{
    border:var(--border);
    box-shadow: var(--box-shadow);
    border-radius: .5rem;
    padding:2.5rem;
    background: #fff;
    text-align: center;
    position: relative;
    overflow: hidden;
    z-index: 0;
}

.review .box-container .box img{
    height: 10rem;
    width: 10rem;
    border-radius: 50%;
    object-fit: cover;
    border:.5rem solid #fff;
}

.review .box-container .box h3{
    color:#fff;
    font-size: 2.2rem;
    padding:.5rem 0;
}

.review .box-container .box .stars i{
    color:#fff;
    font-size: 1.5rem;
}

.review .box-container .box .text{
    color:var(--light-color);
    line-height: 1.8;
    font-size: 1.6rem;
    padding-top: 4rem;
}

.review .box-container .box::before{
    content: '';
    position: absolute;
    top:-4rem; left: 50%;
    transform:translateX(-50%);
    background:var(--green);
    border-bottom-left-radius: 50%;
    border-bottom-right-radius: 50%;
    height: 25rem;
    width: 120%;
    z-index: -1;
}

.blogs .box-container{
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(30rem, 1fr));
    gap:2rem;
}

.blogs .box-container .box{
    border:var(--border);
    box-shadow: var(--box-shadow);
    border-radius: .5rem;
    padding: 2rem;
}

.blogs .box-container .box .image{
    height: 20rem;
    overflow:hidden;
    border-radius: .5rem;
}

.blogs .box-container .box .image img{
    height: 100%;
    width: 100%;
    object-fit: cover;
}

.blogs .box-container .box:hover .image img{
    transform:scale(1.2);
}

.blogs .box-container .box .content{
    padding-top: 1rem;
}

.blogs .box-container .box .content .icon{
    padding: 1rem 0;
    display: flex;
    align-items: center;
    justify-content: space-between;
}

.blogs .box-container .box .content .icon a{
    font-size: 1.4rem;
    color: var(--light-color);
}

.blogs .box-container .box .content .icon a:hover{
    color:var(--green);
}

.blogs .box-container .box .content .icon a i{
    padding-right: .5rem;
    color: var(--green);
}

.blogs .box-container .box .content h3{
    color:var(--black);
    font-size: 3rem;
}

.blogs .box-container .box .content p{
    color:var(--light-color);
    font-size: 1.5rem;
    line-height: 1.8;
    padding:1rem 0;
}

.footer .box-container{
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(22rem, 1fr));
    gap:2rem;
}

.footer .box-container .box h3{
    font-size: 2.5rem;
    color:var(--black);
    padding: 1rem 0;
}

.footer .box-container .box a{
    display: block;
    font-size: 1.5rem;
    color:var(--light-color);
    padding: 1rem 0;
}

.footer .box-container .box a i{
    padding-right: .5rem;
    color:var(--green);
}

.footer .box-container .box a:hover i{
    padding-right:2rem;
}

.footer .credit{
    padding: 1rem;
    padding-top: 2rem;
    margin-top: 2rem;
    text-align: center;
    font-size: 2rem;
    color:var(--light-color);
    border-top: .1rem solid rgba(0, 0, 0, .1);
}

.footer .credit span{
    color:var(--green);
}









/* media queries  */
@media (max-width:991px){

    html{
        font-size: 55%;
    }

    .header{
        padding: 2rem;
    }

    section{
        padding:2rem;
    }

}

@media (max-width:768px){

    #menu-btn{
        display: initial;
    }

    .header .navbar{
        position: absolute;
        top:115%; right: 2rem;
        border-radius: .5rem;
        box-shadow: var(--box-shadow);
        width: 30rem;
        border: var(--border);
        background: #fff;
        transform: scale(0);
        opacity: 0;
        transform-origin: top right;
        transition: none;
    }

    .header .navbar.active{
        transform: scale(1);
        opacity: 1;
        transition: .2s ease-out;
    }

    .header .navbar a{
        font-size: 2rem;
        display: block;
        margin:2.5rem;
    }

}

@media (max-width:450px){

    html{
        font-size: 50%;
    }

}
</style>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> PR SPA&SALONS </title>
    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <!-- custom css file link  -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body bgcolor="skyblue">
<!-- header section starts  -->
<header class="header">
    <a href="#" class="logo"><i class='fas fa-cannabis'></i>PR SPA&SALONS </a>
    <nav class="navbar">
        <a href="#home">HOME</a>
        <a href="#services">SERVICES</a>
        <a href="#about">ABOUT</a>
        <a href="#book your service">BOOK YOUR SERVICE</a>
        <a href="#review">REVIEW</a>
        <a href="#contact">CONTACT</a>
    </nav>
    <div id="menu-btn" class="fas fa-bars"></div>
</header>
<!-- header section ends -->
<!-- home section starts  -->
<section class="home" id="home">
    <div class="content">
    <center>
       <b><h1>Online salon & spa booking and appointment management</h1></b>
        </center>
        <p>A salon appointment booking app is an extension of the appointment book. It allows our clients the freedom and convenience to make an appointment at any moment and from any device, even outside of working hours. You don’t have to worry about taking an appointment by calling the salon in the working hours from 10 am to 6 pm. Online salon booking makes booking appointments easier for you and helps you save time by not letting you wait in queues.</p>
            <p>In this day and age, salon booking programs are not a fancy add-on, but a necessity in modern customer service.</p>
    </div>
</section></br>
</br>
<!-- home section ends -->
<!-- icons section starts  -->
<h1><marquee behavior="scroll" direction="right">Your beauty goals are in good hands when you entrust them to our dedicated experts.</marquee></h1>
</br>
</br>
<section class="icons-container">
    <div class="icons">
        <i class="fas fa-user" aria-hidden="true"></i>
        <h3>50+</h3>
        <p>BEAUTICIANS</p>
    </div>
    <div class="icons">
        <i class="fas fa-users"></i>
        <h3>200+</h3>
        <p>SATISFIED CLIENTS</p>
    </div>
    <div class="icons">
        <i class="fa fa-bed" aria-hidden="true"></i>
        <h3>60+</h3>
        <p>SPA ROOMS</p>
    </div>
</section>
<!-- icons section ends -->
<!-- services section starts  -->
<section class="services" id="services">
    <h1 class="heading"> our <span>services</span> </h1>
    <div class="box-container">
        <div class="box">
            <h3>PEDICURE & MANICURE</h3>
            <p>Pedicure and manicure are both cosmetic treatments that improve the look of your nails, hands and feet. Pedicure refers to a treatment of the toenails and feet; manicures are the fingernails and hands.</p>
        </div>
        <div class="box">
            <h3>NAIL EXTENSION</h3>
            <p>Nail extensions involve the addition of an artificial tip to the tip of your nail to increase the length. The appendix is then strengthened by covering it in gel,acrylic,or fibreglass.</p>
        
        </div>
        <div class="box">
            <h3>AROMATHERAPHY MASSAGE</h3>
            <p>It ia a popular item on a spa's menu. In this type of treatment,fragnent therapeutic essential oils are used to activatehealing properties in your body.</p>
           
        </div>
        <div class="box">
            <h3>FACIAL MASSAGE</h3>
            <p>A facial is a basic massage treatment, including your face, neck, and back area. Moreover, it contains exfoliation, cleansing, face pack, steam, peeling, and toning. Properly performed facial massage is highly beneficial for all the beauty issues and makes your skin looks younger and brighter.</p>
            
        </div>
        <div class="box">   
            <h3>HAIR-SPA</h3>
            <p>Hair spa is not only treatment but therapy for your hair. It helps you retain the shine and softness of your hair while keeping your hair healthy. Treating your hair regularly with a hair spa can maintain the moisture and sheen of your hair.</p>
         
        </div>
        <div class="box">
            <h3>THREADING & WAXING</h3>
            <p>Waxing and threading are quite different in their hair removal approach. Threading is much more of a small-scale operation, whereas waxing can be completed on large sections. The pain of each varies and affects people differently.</p>
            
        </div>
    </div>
</section>
<!-- services section ends -->

<!-- about section starts  -->

<section class="about" id="about">

    <h1 class="heading"> <span>about</span> us </h1>

    <div class="row">

        <div class="image">
                <image src="image/about-img.jpg" alt=""> 
        </div>

        <div class="content">
            <h3 style="color:Tomato;"><s>Luxury Spa & Beauty Salon</s></h3>
            <p>Our beauty salon offers a stunning complement of therapeutic and rejuvenating face and body beauty treatments, using only the best skincare products with the finest ingredients.</p></br></br></br>
            <p>The Spa Beauty Salon team of experienced and highly trained beauty therapists will be happy to put together a spa or beauty treatment package designed specifically for your needs and to offer you advice on your skin’s requirements. Within the Spa Beauty Salon oasis of calm and tranquility, begin your journey of self-discovery away from the outside world where body, mind and soul are nourished and restored, leaving you energised from within and looking great.</p>
           
        </div>

    </div>

</section>

<!-- about section ends -->

<!-- booking section starts   -->

<section class="book your sevice" id="book your service">

    <h1 class="heading"> <span>book</span>YOUR SERVICE </h1>    

    <div class="row">

        <div class="image">
            <img src="https://estheva.com/wp-content/uploads/2015/08/Spa_Booking_Online_Singapore.jpg" alt="">        </div>

        <form action="view/success.jsp">
            <h3>book appointment</h3>
            <input type="text" placeholder="your name" class="box">
            <input type="number" placeholder="your number" class="box">
            <input type="email" placeholder="your email" class="box">
            <input type="date" class="box">
            <input type="submit" value="book now" class="btn">  
            
                      
        </form>

    </div>

</section>

<!-- booking section ends -->

<!-- review section starts  -->

<section class="review" id="review">
    
    <h1 class="heading"> client's <span>review</span> </h1>

    <div class="box-container">

<div class="box">
            <h3>LIKITHA</h3>
            <div class="stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star-half-alt"></i>
            </div>
            <p class="text">Excellent spa services. Trained and experienced staff, spacious and hygienic. Great customer service and reasonable packages.</p>
        </div>

        <div class="box">
        
            <h3>MEGHA</h3>
            <div class="stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star-half-alt"></i>
            </div>
            <p class="text">Wonderful experience! Staff are all so helpful and friendly. Thank you to their beauticans for such an amazing service and generally making the experience so easy despite the pain of the treatment. Thank you so much for being honest and letting me know what’s best for my skin. I’ve been advised to give my skin a break from treatments and it shows how much she genuinely cares about what’s best for me rather than pushing me 
                towards constantly booking in treatments. I look forward to seeing you again in 6 months.</p>
         </div>

        <div class="box">
            
            <h3>MASAKO</h3>
            <div class="stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star-half-alt"></i>
            </div>
            <p class="text">The Spa Beauty Salon is a lovely salon with very friendly and welcoming staff. There was a nice seating area and the whole place was really clean. My appointment was on time and I came a little earlier to look at the colours (which they have hundreds of!). Their beauticians are very knowledgeable and gave great advice about nail care and after care which I hadn’t received from previous salons – I learned a lot! Everything was done very professionally (and beautifully) and I wouldn’t go anywhere else now! Thankyou for a lovely experience!

                Nail Salon Client</p>
        </div>

    </div>

</section>

<!-- review section ends -->

<!-- contact section starts  -->

<section class="contact" id="contact">

    <h1 class="heading"> our<span>CONTACT</span></h1>
     <center>
        <div class="image">
            <img src="image/contactimg.jpg" alt="">
        </div>
    <h2>
        <div class="box">
            <h3>contact info</h3>
            <a href="#"> <i class="fas fa-phone"></i> +123-456-7890 </a> </br>
            <a href="#"> <i class="fas fa-phone"></i> +111-222-3333 </a></br>
            <a href="#"> <i class="fas fa-envelope"></i> prspa&services@gmail.com </a></br>
            <a href="#"> <i class="fas fa-envelope"></i> pr@gmail.com </a></br>
            <a href="#"> <i class="fas fa-map-marker-alt"></i> Hyderabad, india - 400104 </a>
        </div>

    </h2>

</center>
</section>

<!-- contact section ends -->

<!-- footer section starts  -->

<section class="footer">

    <div class="box-container">

        <div class="box">
            <h3>quick links</h3>
            <a href="#"> <i class="fas fa-chevron-right"></i> HOME </a>
            <a href="#"> <i class="fas fa-chevron-right"></i> SERVICES </a>
            <a href="#"> <i class="fas fa-chevron-right"></i> ABOUT </a>
            <a href="#"> <i class="fas fa-chevron-right"></i> BOOK YOUR SERVICE </a>
            <a href="#"> <i class="fas fa-chevron-right"></i> REVIEW </a>
            <a href="#"> <i class="fas fa-chevron-right"></i> CONTACT </a>
        </div>

        <div class="box">
            <h3>our services</h3>
            <a href="#"> <i class="fas fa-chevron-right"></i> PEDICURE & MANICURE </a>
            <a href="#"> <i class="fas fa-chevron-right"></i> NAIL EXTENSION </a>
            <a href="#"> <i class="fas fa-chevron-right"></i> AROMATHERAPHY MASSAGE </a>
            <a href="#"> <i class="fas fa-chevron-right"></i> FACIAL MASSAGE</a>
            <a href="#"> <i class="fas fa-chevron-right"></i> HAIR-SPA</a>
            <a href="#"> <i class="fas fa-chevron-right"></i> THREADING&WAXING </a>
        </div>

        <div class="box">
            <h3>contact info</h3>
            <a href="#"> <i class="fas fa-phone"></i> +123-456-7890 </a>
            <a href="#"> <i class="fas fa-phone"></i> +111-222-3333 </a>
            <a href="#"> <i class="fas fa-envelope"></i> prspa&services@gmail.com </a>
            <a href="#"> <i class="fas fa-envelope"></i> pr@gmail.com </a>
            <a href="#"> <i class="fas fa-map-marker-alt"></i> Hyderabad, india - 400104 </a>
        </div>

        <div class="box">
            <h3>follow us</h3>
            <a href="#"> <i class="fab fa-facebook-f"></i> facebook </a>
            <a href="#"> <i class="fab fa-twitter"></i> twitter </a>
            <a href="#"> <i class="fab fa-youtube"></i> youtube </a>
            <a href="#"> <i class="fab fa-instagram"></i> instagram </a>
            <a href="#"> <i class="fab fa-pinterest"></i> pinterest </a>
           <a href="#"><i class="fa fa-qrcode" aria-hidden="true"></i> QR CODE</a>
        </div>
    </div>
    <div class="credit"> created by </br>----------------| <span> PR SPA&SALONS</span> |---------------- </br> all rights reserved </div>
</section>
<!-- footer section ends -->
<!-- custom js file link  -->
<script src="js/script.js"></script>
</body>
</html>
</html>