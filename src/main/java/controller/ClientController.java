package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import dao.IClient;
import dao.ClientImpl;
import model.Client;

@Controller
public class ClientController {
	IClient client = new ClientImpl();
//	Client p=new Client();
	@RequestMapping("views")
	public ModelAndView views() {
		
		return new ModelAndView("views", "clientref", client.getAllclients()); // view-success page i.e jsp page//

	}
	@RequestMapping("update")
	public ModelAndView update(@RequestParam(name = "id") int id, @RequestParam(name = "name") String name) {
		ModelAndView modelAndView = new ModelAndView("update");
		modelAndView.addObject("id", id);
		modelAndView.addObject("name", name);
		modelAndView.addObject("obj", new Client());
		return modelAndView;

	}

	@RequestMapping("update1")
	public ModelAndView update1(@ModelAttribute(name = "obj") Client p) {
		return new ModelAndView("ack", "ref", client.update(p));
	}
	@RequestMapping("home")
	public ModelAndView home() {
		return new ModelAndView("home");
	}
	@RequestMapping("login")
	public ModelAndView login() {
		return new ModelAndView("views");
	}
	@RequestMapping("delete")
	public ModelAndView delete(@RequestParam(name="id")int id) {
		return new ModelAndView("ack","ref", client.delete(id));
	}
	@RequestMapping("insert")
	public ModelAndView insert() {
		return new ModelAndView("insert", "client", new Client());
	}
	@RequestMapping("add")
	public ModelAndView add(@ModelAttribute("client")Client p) {
		return new ModelAndView("ack", "ref", client.register(p));
	}
	@RequestMapping("contact")
	public ModelAndView contact() {
		return new ModelAndView("contact");
	}
	@RequestMapping("about")
	public ModelAndView about() {
		return new ModelAndView("about");
	}
	
	@RequestMapping("indexes")
	public ModelAndView indexes() {
		return new ModelAndView("indexes");
}
}
