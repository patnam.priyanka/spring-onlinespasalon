package dao;

import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import model.Client;

public class ClientImpl implements IClient {
	@SuppressWarnings("unchecked")

	@Override
	public List<Client> getAllclients() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		List<Client> list = manager.createQuery("from Client").getResultList();
		manager.close();
		factory.close();
		return list;

	}

	public String update(Client client) {
		// JPA code
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		manager.merge(client);// update
		manager.getTransaction().commit();
		manager.close();
		factory.close();
		return client.getId() + "Updated Successfully";
	}
	@Override
	public String delete(int id) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		Client p = manager.getReference(Client.class,id);
		manager.remove(p);
		manager.getTransaction().commit();
		manager.close();
		factory.close();
		return id + "Deleted Successfully";
		
	}
	@Override
	public String register(Client client) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		manager.persist(client);
		manager.getTransaction().commit();
		manager.close();
		factory.close();
		return client.getId() + "Inserted Scuccessfully";
	}

}
