package dao;

import java.util.List;
import model.Client;

public interface IClient {
	public List<Client> getAllclients();

	public String update(Client client);
	public String delete(int id);
	public String register(Client client);
}
